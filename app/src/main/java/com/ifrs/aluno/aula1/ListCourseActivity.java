package com.ifrs.aluno.aula1;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.ifrs.aluno.aula1.model.Courses;
import com.ifrs.aluno.aula1.model.User;
import com.ifrs.aluno.aula1.service.CursosAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ListCourseActivity extends AppCompatActivity {
    ListView l;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_course);
        l = (ListView) findViewById(R.id.listview);
        getCoursesApi();

    }

    private void getCoursesApi() {
        String service = "core_enrol_get_users_courses";
        String uri = "http://moodle.canoas.ifrs.edu.br/webservice/rest/server.php";

        uri +="?wsfunction=" + service+ "&wstoken=" + getIntent().getStringExtra("token") + "&moodlewsrestformat=json&userid=720";
        Log.d("DEBUG Courses", "url: " + uri);
        new DownloadWebpageTask().execute(uri);



    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid." + e;
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            Gson g = new Gson();


            Courses[]  courses = g.fromJson(result, Courses[].class);

            List<Courses>  lisc = new ArrayList<Courses>();
            for(Courses c : courses) {
                Log.d("s",c.getFullname());
                lisc.add(c);

            }
            Log.d("s",lisc.get(1).toString());
            CursosAdapter  adapter = new CursosAdapter(getApplicationContext(),lisc);
            l.setAdapter(adapter);


        }
    }


    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 10000;
        Log.d("DEBUG", "url: " + myurl);

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("DEBUG", "Resposta HTTP: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is, len);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {

            if (is != null) {
                is.close();
            }
        }

    }

    public String readIt(InputStream stream, int len)
            throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream,"UTF-8");
        char[] buffer = new char[7666];
        //reader.read(buffer);
        int count;
        String str = "";
        while ((count = reader.read(buffer)) > 0) { str += new String(buffer, 0, count); }
        Log.d("json", ""  + str);
        return str;
    }

}
