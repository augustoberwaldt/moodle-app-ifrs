package com.ifrs.aluno.aula1.model;


public class Courses {


    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private String fullname;


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        return "Courses{" +
                "fullname='" + fullname + '\'' +
                '}';
    }
}
