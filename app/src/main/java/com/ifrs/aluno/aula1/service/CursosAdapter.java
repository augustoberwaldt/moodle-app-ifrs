package com.ifrs.aluno.aula1.service;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifrs.aluno.aula1.R;
import com.ifrs.aluno.aula1.model.Courses;

import java.util.List;

public class CursosAdapter extends BaseAdapter {
    private Context context;
    private List<Courses> list;

    public CursosAdapter(Context context, List<Courses> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return list.get(arg0).getId();
    }

    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {
        final int auxPosition = position;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.curso_linha, null);

        TextView tv = (TextView) layout.findViewById(R.id.curso_nome);
        tv.setText(list.get(position).getFullname());



        return layout;
    }

}
