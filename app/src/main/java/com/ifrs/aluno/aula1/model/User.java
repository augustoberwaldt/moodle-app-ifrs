package com.ifrs.aluno.aula1.model;

/**
 * Created by Aluno on 06/06/2017.
 */

public class  User {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
